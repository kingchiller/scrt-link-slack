import { App } from '@slack/bolt'

import { blockCreateSecretCallback } from './block_button-create-secret'
import { blockChangeSecretTypeCallback } from './block_radio-change-secret-type'

const blockCreateSecretListener = (app: App) => {
  app.action(
    { action_id: 'app-home-create-secret', type: 'block_actions' },
    blockCreateSecretCallback,
  )
}

const blockChangeSecretTypeListener = (app: App) => {
  app.action(
    { action_id: 'radio-switch-secret-type', type: 'block_actions' },
    blockChangeSecretTypeCallback,
  )
}

export default {
  blockAppHomeCreateSecret: blockCreateSecretListener,
  modalCreateSecretSwitchType: blockChangeSecretTypeListener,
}
