import { Middleware, SlackAction, SlackActionMiddlewareArgs } from '@slack/bolt'

import modals from '../../modals'

export const blockCreateSecretCallback: Middleware<
  SlackActionMiddlewareArgs<SlackAction>
> = async ({ ack, client, body }) => {
  await ack()

  await client.views.open({
    //@ts-ignore
    trigger_id: body.trigger_id, // @todo fix type
    view: modals.createSecret({ showMenu: true }),
  })
}
