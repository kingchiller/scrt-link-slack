import {
  Middleware,
  SlackActionMiddlewareArgs,
  BlockRadioButtonsAction,
} from '@slack/bolt'
import { SecretType } from 'scrt-link-core/dist/tsc/types'

import modals from '../../modals'
import { CreateSecretMeta } from '../../modals/createSecret'

export const blockChangeSecretTypeCallback: Middleware<
  SlackActionMiddlewareArgs<BlockRadioButtonsAction>
> = async ({ ack, client, body, action }) => {
  await ack()

  // Here we extract privateMetaData to make sure "recipient" is passed along.
  const metaData = JSON.parse(
    body?.view?.private_metadata as string,
  ) as CreateSecretMeta

  await client.views.update({
    //@ts-ignore
    view_id: body.view.id,
    view: modals.createSecret({
      showMenu: true,
      recipient: metaData?.recipient,
      secretType: (action?.selected_option?.value as SecretType) || 'text',
    }),
  })
}
