import { Middleware, SlackEventMiddlewareArgs } from '@slack/bolt'
import { HomeTab, Elements, Blocks, Md } from 'slack-block-builder'

import { baseUrl } from '../../constants'

const secretCommandText = (text: string) => Md.bold(Md.codeInline(text))
const homeView = HomeTab()
  .blocks(
    Blocks.Image({
      imageUrl: `${baseUrl}/og-image.png`,
      altText: 'Scrt.link - Share a secret!',
    }),
    Blocks.Section({
      text: `Use this app to *share confidential information within Slack 🤫*.  `,
    }),
    Blocks.Section({
      text: `Sensitive information, such as _passwords_, _PIN codes_, _access tokens_ as well as _confessions_ and other _secrets_, should not be part of your chat history. This app offers three types of secrets: *Text* (Default), *Link* and *Neogram*. Read more about the characteristics of each secret type below.`,
    }),

    Blocks.Header({ text: 'How to use' }),
    Blocks.Section({ text: 'There are various ways of using this app:' }),
    Blocks.Section({
      text: `*1️⃣ Use the \`/scrt\` command*:
This is probably the easiest and most versatile option.

${Md.listBullet(
  `${secretCommandText('/scrt')} opens a dialog to create a secret.`,
)}  

${Md.listBullet(
  `${secretCommandText(
    `/scrt [secret goes here…]`,
  )} creates a secret link instantly (of type *Text*). Example: ${Md.codeInline(
    '/scrt I love you!',
  )}`,
)}  

${Md.listBullet(
  `${secretCommandText('/scrt text')}, ${secretCommandText(
    '/scrt link',
  )} or ${secretCommandText(
    '/scrt neogram',
  )} opens a dialog to create a specific type of secret.`,
)}  

${Md.listBullet(`${secretCommandText('/scrt help')} Opens help dialog.`)}  
`,
    }),
    Blocks.Header({ text: '‎' }), // Make space with unicode char

    Blocks.Section({
      text: `*2️⃣ Use global shortcuts*:
> ${Md.listBullet('Click ⚡️ and select *Scrt.link* > *Create a secret*')}`,
    }),
    Blocks.Image({
      imageUrl: `${baseUrl}/screenshot-shortcuts.png`,
      altText: 'Slack shortcuts',
    }),
    Blocks.Header({ text: '‎' }), // Make space with unicode char

    Blocks.Section({
      text: `*3️⃣ Use context menu shortcuts*:
> ${Md.listBullet(
        'Use the context menu within a conversation and click the "*Reply with a secret*" option.',
      )}`,
    }),
    Blocks.Image({
      imageUrl: `${baseUrl}/screenshot-message-shortcuts.png`,
      altText: 'Slack message shortcuts',
    }),
    Blocks.Header({ text: '‎' }), // Make space with unicode char

    Blocks.Header({ text: 'What happens in the background?' }),
    Blocks.Section({
      text: `
${Md.listBullet(
  'After you submit your secret, it gets encrypted and stored outside of Slack.',
)} 
${Md.listBullet(
  'The Slack bot then posts a secret link to the original conversation.',
)} 
${Md.listBullet(
  'Once the link has been clicked by the recipient, the secret gets revealed while being destroyed at the same time (no backup).',
)} 
${Md.listBullet(
  `The message containing the secret link get's marked with a 🔥 emoji to indicate that it has been viewed. It's now no longer available.`,
)}`,
    }),

    Blocks.Header({
      text: 'What is the difference between Text, Link and Neogram™?',
    }),
    Blocks.Section({
      text: `
${Md.listBullet(
  `${Md.bold(
    'Text',
  )}: This is the standard mode. It's the preferred way to share passwords and similar kind of secrets. The recipient has the option to copy the secret. ${Md.link(
    'https://scrt.link/l/preview?preview=%7B%22message%22%3A%22Hi%20there%E2%80%A6%5CnThis%20is%20just%20a%20demo!%5Cn%5CnEnjoy%20sharing%20secrets%20%F0%9F%98%98%22%2C%22secretType%22%3A%22text%22%7D',
    'Demo',
  )}`,
)} 
${Md.listBullet(
  `${Md.bold(
    'Link',
  )}: Think about it as a URL-shortener where the generated link only works once.`,
)} 
${Md.listBullet(
  `${Md.bold(
    'Neogram™',
  )}: Digital letter-style message that automatically burns after reading. Use it for confidential notes, confessions or secret love letters. ${Md.link(
    'https://scrt.link/l/preview?preview=%7B%22message%22%3A%22Hi%20there%E2%80%A6%5CnThis%20is%20just%20a%20demo!%20Neogram%20messages%20self-destruct%20automatically%20after%20a%20defined%20period%20of%20time.%5Cn%5CnEnjoy%20sharing%20secrets%20%F0%9F%98%98%22%2C%22secretType%22%3A%22neogram%22%2C%22neogramDestructionMessage%22%3A%22This%20message%20self-destructs%20in%20%E2%80%A6%22%2C%22neogramDestructionTimeout%22%3A3%7D',
    'Demo',
  )}`,
)} `,
    }),

    Blocks.Header({ text: 'Try it now' }),
    Blocks.Section({ text: 'Go ahead and create your first secret!' }),
    Blocks.Actions().elements(
      Elements.Button({
        text: 'Create a Secret',
        actionId: 'app-home-create-secret',
      }).primary(),
    ),
  )
  .buildToObject()

export const appHomeOpenedCallback: Middleware<
  SlackEventMiddlewareArgs<'app_home_opened'>
> = async ({ event, client }) => {
  client.views.publish({
    user_id: event.user,
    view: homeView,
  })
}
