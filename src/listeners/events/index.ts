import { App } from '@slack/bolt'

import { appHomeOpenedCallback } from './app_home_opened'

const appHomeOpenedListener = (app: App) =>
  app.event('app_home_opened', appHomeOpenedCallback)

export default {
  appHomeOpened: appHomeOpenedListener,
}
