import { Middleware, SlackCommandMiddlewareArgs } from '@slack/bolt'
import { MongoClient } from 'mongodb'

import modals from '../../modals'
import {
  getUserTokenByUserId,
  handleSecretCreation,
  getRecipientChannelId,
} from '../../utils/helpers'
import { appInstallUrl } from '../../constants'

export const scrtCallback =
  (mongoClient: MongoClient): Middleware<SlackCommandMiddlewareArgs> =>
  async ({ command, ack, respond, client, body }) => {
    // Acknowledge command request
    await ack()

    console.log(body)

    // Extract information from command payload
    const userId = body.user_id
    const userName = body.user_name
    const recipient = body.channel_id

    // Extract token from installation store
    const userToken = await getUserTokenByUserId(mongoClient, userId)

    if (!userToken) {
      await respond(`You need to authorize the app first: ${appInstallUrl}`)
      return
    }

    // Check for special commands that trigger modal view
    if (command.text === '') {
      return await client.views.open({
        trigger_id: command.trigger_id,
        view: modals.createSecret({ showMenu: true, recipient }),
      })
    }

    if (command.text === 'text') {
      return await client.views.open({
        trigger_id: command.trigger_id,
        view: modals.createSecret({ secretType: 'text', recipient }),
      })
    }

    if (command.text === 'link') {
      return await client.views.open({
        trigger_id: command.trigger_id,
        view: modals.createSecret({ secretType: 'url', recipient }),
      })
    }

    if (command.text === 'neogram') {
      return await client.views.open({
        trigger_id: command.trigger_id,
        view: modals.createSecret({
          secretType: 'neogram',
          recipient,
        }),
      })
    }

    if (command.text === 'help') {
      return await client.views.open({
        trigger_id: command.trigger_id,
        view: modals.getHelp(),
      })
    }

    // In case there is no special command we create a secret with the command text right away.
    try {
      const channelId = await getRecipientChannelId({
        client,
        recipient,
        userToken,
      })

      console.log(channelId)

      if (!channelId) {
        throw Error('No channel id!')
      }
      await handleSecretCreation({
        client,
        mongoClient,
        message: command.text,
        channelId: channelId,
        userToken,
        userId,
        userName,
      })
    } catch (error) {
      console.error(
        `Couldn't create secret link: ${
          error instanceof Error ? error.message : 'No error message'
        }`,
      )
      return await respond(
        `Something went wrong. You may try to reinstall the app: ${appInstallUrl}. If the issue persists, please contact support: support@scrt.link`,
      )
    }
  }
