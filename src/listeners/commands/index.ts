import { App } from '@slack/bolt'
import { MongoClient } from 'mongodb'

import { scrtCallback } from './scrt'

const scrtCommandListener = (app: App, mongoClient: MongoClient) =>
  app.command('/scrt', scrtCallback(mongoClient))

export default {
  scrt: scrtCommandListener,
}
