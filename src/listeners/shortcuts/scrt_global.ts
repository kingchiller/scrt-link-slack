import { Middleware, SlackShortcutMiddlewareArgs } from '@slack/bolt'

import modals from '../../modals'

const callback: Middleware<SlackShortcutMiddlewareArgs> = async ({
  ack,
  respond,
  say,
  shortcut,
  payload,
  body,
  client,
}) => {
  // Acknowledge command request

  try {
    await ack()
    await client.views.open({
      trigger_id: shortcut.trigger_id,
      view: modals.createSecret({ showMenu: true }),
    })
  } catch (error) {
    // eslint-disable-next-line no-console
    console.error(error)
  }
}

export default callback
