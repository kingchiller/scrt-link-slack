import {
  MessageShortcut,
  Middleware,
  SlackShortcutMiddlewareArgs,
} from '@slack/bolt'

import modals from '../../modals'

const callback: Middleware<SlackShortcutMiddlewareArgs<MessageShortcut>> =
  async ({ ack, respond, shortcut, body, client }) => {
    try {
      await ack()
      // Get channel id
      const recipient = body.channel.id

      await client.views.open({
        trigger_id: shortcut.trigger_id,
        view: modals.createSecret({ recipient: recipient }),
      })
    } catch (error) {
      await respond(error instanceof Error ? error.message : 'Error')
    }
  }

export default callback
