import { App } from '@slack/bolt'

import callbackScrtGlobal from './scrt_global'
import callbackScrtMessage from './scrt_message'

const scrtGlobalShortcutListener = (app: App) =>
  app.shortcut('scrt_global', callbackScrtGlobal)

const scrtMessageShortcutListener = (app: App) =>
  app.shortcut('scrt_message', callbackScrtMessage)

export default {
  scrtGlobal: scrtGlobalShortcutListener,
  scrtMessage: scrtMessageShortcutListener,
}
