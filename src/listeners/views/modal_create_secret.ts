import { Middleware, SlackViewMiddlewareArgs } from '@slack/bolt'
import { pathOr, path } from 'ramda'
import * as yup from 'yup'
import { MongoClient } from 'mongodb'
import { SecretType } from 'scrt-link-core/dist/tsc/types'

import {
  getUserTokenByUserId,
  handleSecretCreation,
  getRecipientChannelId,
} from '../../utils/helpers'
import { CreateSecretMeta } from '../../modals/createSecret'
import { appInstallUrl } from '../../constants'
import modals from '../../modals'

export const neogramDestructionMessageDefault =
  'This message will self-destruct in…'

export const createSecretModalCallback =
  (mongoClient: MongoClient): Middleware<SlackViewMiddlewareArgs> =>
  async ({ ack, view, body, client }) => {
    // Extract user information (sender)
    const userId = body?.user?.id
    const userName = body?.user.name

    // Here we extract privateMetaData to make sure "recipient" is passed along.
    const metaData = JSON.parse(
      body?.view?.private_metadata as string,
    ) as CreateSecretMeta

    // Extract form values
    const values = view.state.values
    const recipient =
      metaData?.recipient ||
      (path(
        ['recipient', 'recipient', 'selected_conversation'],
        values,
      ) as string)
    const secretType = pathOr(
      'text',
      ['secretType', 'radio-switch-secret-type', 'selected_option', 'value'],
      values,
    ) as SecretType
    const message = values?.message?.message?.value || ''
    const password = values.password.password.value
    const neogramDestructionMessage =
      values?.neogramDestructionMessage?.neogramDestructionMessage.value
    const neogramDestructionTimeout = pathOr(
      5,
      [
        'neogramDestructionTimeout',
        'neogramDestructionTimeout',
        'selected_option',
        'value',
      ],
      values,
    )

    // Add error in case recipient is missing. This should never happen.
    if (!recipient) {
      await ack({
        response_action: 'push',
        view: modals.showError({
          message: `Error. No recipient specified.`,
        }),
      })
      return
    }

    // Form validation
    if (secretType === 'url' && !(await yup.string().url().isValid(message))) {
      await ack({
        response_action: 'errors',
        errors: {
          message: 'Please provide proper URL.',
        },
      })
      return
    }

    // We get the user token from the installation store.
    // In case user token doesn't exist we urge user to re-install the app.
    const userToken = await getUserTokenByUserId(mongoClient, userId)
    if (!userToken) {
      await ack({
        response_action: 'push',
        view: modals.showError({
          message: `You need to authorize the app first: ${appInstallUrl}`,
        }),
      })
      return
    }

    try {
      const channelId = await getRecipientChannelId({
        client,
        recipient,
        userToken,
      })

      await handleSecretCreation({
        client,
        mongoClient,
        message: message,
        channelId: channelId,
        userToken: userToken,
        userId: userId,
        userName: userName,
        secretType: secretType as SecretType,
        password: password as string | undefined,
        ...(secretType === 'neogram'
          ? {
              neogramDestructionMessage:
                neogramDestructionMessage || neogramDestructionMessageDefault,
              neogramDestructionTimeout,
            }
          : {}),
      })

      await ack()
      return
    } catch (error) {
      await ack({
        response_action: 'push',
        view: modals.showError({
          message: `Error. ${
            error instanceof Error ? error.message : 'Not sure what went wrong.'
          }`,
        }),
      })
      return
    }
  }
