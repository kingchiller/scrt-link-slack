import { App } from '@slack/bolt'
import { MongoClient } from 'mongodb'
import { createSecretModalCallback } from './modal_create_secret'

const createSecretModalListener = (app: App, mongoClient: MongoClient) =>
  app.view('modal_create_secret', createSecretModalCallback(mongoClient))

export default {
  createSecretModal: createSecretModalListener,
}
