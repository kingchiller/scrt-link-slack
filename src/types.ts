declare global {
  namespace NodeJS {
    interface ProcessEnv {
      SLACK_BOT_TOKEN: string
      SLACK_SIGNING_SECRET: string
      SLACK_CLIENT_ID: string
      SLACK_CLIENT_SECRET: string
      SLACK_STATE_SECRET: string
      DB: string
    }
  }
}

export {}
