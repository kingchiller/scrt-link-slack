/* eslint-disable no-console */
/* eslint-disable import/no-internal-modules */

import { App, Installation, LogLevel, ExpressReceiver } from '@slack/bolt'
import { MongoClient } from 'mongodb'
import express from 'express'

import { upsertListingById, dbFindOne, dbDeleteOne } from './utils/db'
import './utils/env'
import {
  eventsListener,
  commandsListener,
  shortcutsListener,
  actionsListener,
  viewsListener,
} from './listeners'

const receiver = new ExpressReceiver({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  clientId: process.env.SLACK_CLIENT_ID,
  clientSecret: process.env.SLACK_CLIENT_SECRET,
  stateSecret: process.env.SLACK_STATE_SECRET, // Custom, not provided by Slack
  scopes: ['channels:join', 'chat:write.public', 'chat:write', 'commands'],

  installerOptions: {
    stateVerification: true,
    userScopes: [
      'channels:read', // Required for API: conversations.info
      'groups:read', // Required for API: conversations.info
      'im:read', // Required for API: conversations.info
      'mpim:read', // Required for API: conversations.info
      'channels:write',
      'chat:write',
      'groups:write',
      'im:write',
      'reactions:write',
    ],
    directInstall: true,
  },
  logLevel: LogLevel.DEBUG,
  installationStore: {
    // takes in an installation object as an argument
    // returns nothing
    storeInstallation: async (installation) => {
      if (installation.isEnterpriseInstall && installation.enterprise) {
        await upsertListingById(
          mongoClient,
          { 'enterprise.id': installation.enterprise.id },
          installation,
        )
        return
      }

      if (installation.team) {
        // single team app installation
        await upsertListingById(
          mongoClient,
          { 'team.id': installation.team.id },
          installation,
        )
        return
      }

      throw new Error('Failed saving installation data to installationStore')
    },
    // takes in an installQuery as an argument
    // installQuery = {teamId: 'string', enterpriseId: 'string', userId: 'string', conversationId: 'string', isEnterpriseInstall: boolean};
    // returns installation object from database
    fetchInstallation: async (installQuery) => {
      if (installQuery.isEnterpriseInstall && installQuery.enterpriseId) {
        const result = await dbFindOne(mongoClient, 'installations', {
          'enterprise.id': installQuery.enterpriseId,
        })
        return result as Installation
      }

      if (installQuery.teamId !== undefined) {
        // single team app installation lookup
        const result = await dbFindOne(mongoClient, 'installations', {
          'team.id': installQuery.teamId,
        })
        return result as Installation
      }

      throw new Error('Failed fetching installation')
    },
    // takes in an installQuery as an argument
    // installQuery = {teamId: 'string', enterpriseId: 'string', userId: 'string', conversationId: 'string', isEnterpriseInstall: boolean};
    // returns nothing
    deleteInstallation: async (installQuery) => {
      if (
        installQuery.isEnterpriseInstall &&
        installQuery.enterpriseId !== undefined
      ) {
        // org wide app installation deletion
        await dbDeleteOne(mongoClient, 'installations', {
          'enterprise.id': installQuery.enterpriseId,
        })
        return
      }

      if (installQuery.teamId !== undefined) {
        // single team app installation deletion
        await dbDeleteOne(mongoClient, 'installations', {
          'team.id': installQuery.teamId,
        })
        return
      }
      throw new Error('Failed to delete installation')
    },
  },
})

// Here we need to set ignoreUndefined: true, b/c otherwise undefined values (e.g. enterprise from the installation object) will be transformed to null, causing @slack/oauth to fail.
const mongoClient = new MongoClient(process.env.DB, { ignoreUndefined: true })

const app = new App({
  receiver,
})

// Slack app events and actions
eventsListener.appHomeOpened(app)
commandsListener.scrt(app, mongoClient)
shortcutsListener.scrtGlobal(app)
shortcutsListener.scrtMessage(app)
actionsListener.blockAppHomeCreateSecret(app)
actionsListener.modalCreateSecretSwitchType(app)
viewsListener.createSecretModal(app, mongoClient)

// Custom endpoints
// We use receiver to accept json payloads
receiver.router.use(express.json())

// Status of the app
receiver.router.get('/status', (req, res, next) => {
  res.send(JSON.stringify({ status: 200, message: 'Service operational.' }))
})

// Add public folder for assets
receiver.app.use(express.static('public'))

// Endpoint to post read receipts
receiver.router.post('/receipts', async (req, res, next) => {
  if (req.headers['x-api-key'] !== process.env.SLACK_APP_API_KEY) {
    res.writeHead(401)
    res.end(
      JSON.stringify({ status: 401, message: 'API key is missing or invalid' }),
    )
  }

  type Receipt = {
    receiptId: string
    channel: string
    ts: string
    userId: string
  }

  try {
    const { receiptId } = req.body

    const result = await (<Promise<Receipt>>dbFindOne(mongoClient, 'receipts', {
      receiptId: receiptId,
    }))

    const { channel, ts, userId } = result

    const installation = (await dbFindOne(mongoClient, 'installations', {
      'user.id': userId,
    })) as Installation

    const token = installation.user.token

    // Try to join the channel. Might fail, that's why catch here.
    await app.client.conversations
      .join({ channel: channel, token: token })
      .catch(console.error)

    // Mark viewed link with fire emoji
    await app.client.reactions.add({
      channel: channel,
      name: 'fire',
      timestamp: ts,
      token: token,
    })
    res.send('Read receipt sent!')
  } catch (error) {
    console.error(error)
    res.writeHead(401)
    res.end(
      JSON.stringify({
        status: 401,
        message:
          error instanceof Error
            ? error.message
            : `Couldn't handle read receipt.`,
      }),
    )
  }
})

// Start your app
const startApp = async () => {
  await app.start(Number(process.env.PORT) || 3000)

  console.log('⚡️ Scrt.link Bolt app is running!')
}

startApp()
