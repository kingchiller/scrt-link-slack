import { Installation } from '@slack/bolt'
import { MongoClient } from 'mongodb'
import { omit } from 'ramda'

const db = 'slack_app'

export const dbFindOne = async (
  client: MongoClient,
  collection: string,
  query: Record<string, unknown>,
) => {
  await client.connect()
  const result = await client.db(db).collection(collection).findOne(query)

  // if (result) {
  //   console.log(`Found a listing in the db with the query '${query}':`)
  //   console.log(result)
  // } else {
  //   console.log(`No listings found with query: '${query}'`)
  // }

  return omit(['_id'])(result)
}

export const dbInsertOne = async (
  client: MongoClient,
  collection: string,
  document: Record<string, unknown>,
) => {
  await client.connect()
  const result = await client.db(db).collection(collection).insertOne(document)

  if (result) {
    console.log(`Document was inserted with the id ${result}`)
  }
  return result
}

export const dbDeleteOne = async (
  client: MongoClient,
  collection: string,
  query: Record<string, unknown>,
) => {
  await client.connect()
  const result = await client.db(db).collection(collection).deleteOne(query)

  console.log(`${result.deletedCount} document(s) was/were deleted.`)

  return result
}

// Only used for installations currently.
export const upsertListingById = async (
  client: MongoClient,
  query: Record<string, unknown>,
  updatedInstallation: Installation,
) => {
  await client.connect()

  const collectionInstallations = 'installations'
  const result = await client
    .db(db)
    .collection(collectionInstallations)
    .updateOne(query, { $set: updatedInstallation }, { upsert: true })

  console.log(`${result.matchedCount} document(s) matched the query criteria.`)

  if (result.upsertedCount > 0) {
    console.log(`One document was inserted with the id ${result.upsertedId}`)
  } else {
    console.log(`${result.modifiedCount} document(s) was/were updated.`)
  }
  return result
}
