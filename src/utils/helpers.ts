import { Installation } from '@slack/bolt'
import { WebClient } from '@slack/web-api'
import { MongoClient } from 'mongodb'
import { BlockCollection, Blocks, Md, Message } from 'slack-block-builder'
import { nanoid } from 'nanoid'
import { createSecret } from 'scrt-link-core'
import { CreateSecretOptions } from 'scrt-link-core/dist/tsc/types'

import { dbFindOne, dbInsertOne } from './db'
import { scrtLinkApi } from '../constants'

export const getUserTokenByUserId = async (
  mongoClient: MongoClient,
  userId: string,
) => {
  const installation = (await dbFindOne(mongoClient, 'installations', {
    'user.id': userId,
  })) as Installation
  return installation?.user?.token
}

type ChannelInfoParams = {
  client: WebClient
  recipient: string
  userToken: string
}

export const getRecipientChannelId = async ({
  client,
  recipient,
  userToken,
}: ChannelInfoParams): Promise<string> => {
  return await client.conversations
    .info({
      channel: recipient,
      token: userToken,
    })
    .then(async (data) => {
      if (!data?.channel?.id) {
        throw Error('Channel not found')
      }

      // If bot is not part of the channel, join the channel. (Only works for channels, not DMs - that's why catch here.)
      if (!data?.channel?.is_member) {
        await client.conversations
          .join({
            channel: data?.channel?.id,
            token: userToken,
          })
          .catch((err) => {
            console.error(`Couldn't join conversation: `, err)
          })
      }

      return data.channel.id
    })
    .catch(async (error) => {
      console.error(
        `Couldn't get info of recipient ${recipient} with token ${userToken}: ${
          error instanceof Error ? error.message : 'No error message.'
        }`,
      )

      // If no conversation exists, we try to open a new conversation
      return await client.conversations
        .open({
          users: recipient,
          token: userToken,
        })
        .then(async (data) => {
          if (!data?.channel?.id) {
            throw Error(
              `Couldn't open conversation with recipient: "${recipient}"`,
            )
          }
          return data.channel.id
        })
    })
}

interface HandleSecretCreation extends CreateSecretOptions {
  client: WebClient
  mongoClient: MongoClient
  message: string
  channelId: string
  userToken: string
  userId: string
  userName: string
}
export const handleSecretCreation = async ({
  client,
  mongoClient,
  message,
  channelId,
  userToken,
  userId,
  userName,
  ...props
}: HandleSecretCreation) => {
  const receiptId = nanoid()
  const { secretLink } = await createSecret(
    message,
    {
      receiptApi: { slack: receiptId },
      ...props,
    },
    scrtLinkApi,
  )

  // For DM messages we need to use a user token to send bot messages "as user".
  const botMessage = await client.chat
    .postMessage({
      blocks: BlockCollection(
        Blocks.Section({
          text: `Here is a secret 🤫: ${Md.link(secretLink, secretLink)}`,
        }),
      ),
      channel: channelId,
      token: userToken,
    })
    .catch((error) => {
      console.error(
        `Couldn't post secret link to channel id ${channelId} with token ${userToken}: ${
          error instanceof Error ? error.message : 'No error message.'
        }`,
      )
    })

  // If the secret link was successfully sent we store a reference in the DB for a read receipt.
  if (botMessage?.ok) {
    const { channel, ts } = botMessage
    // Save reference to DB for read receipts
    const receipt = {
      receiptId: receiptId,
      channel,
      ts,
      userId,
    }

    await dbInsertOne(mongoClient, 'receipts', receipt)
  }

  return { ok: true }
}
