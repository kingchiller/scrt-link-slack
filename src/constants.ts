export const baseUrl = 'https://slack.scrt.link' // Dev: https://de5a-2a02-168-5538-0-ad36-d0ef-1a0a-ac69.ngrok.io
export const appInstallUrl = `${baseUrl}/slack/install`
export const scrtLinkApi = 'https://scrt.link' // Stage: https://dev.scrt.link, Dev: http://localhost:3001
