import { Modal, Blocks, Md } from 'slack-block-builder'

const getHelp = () =>
  Modal({
    title: 'Slash commands',
    callbackId: 'help',
  })
    .blocks(
      Blocks.Section({
        text: 'Use the following commands within a conversation to create a secret link.',
      }),
      Blocks.Section({
        text: `
${Md.listBullet(
  `${Md.codeInline('/scrt')} opens a dialog to create a secret.`,
)}  

${Md.listBullet(
  `${Md.codeInline(
    `/scrt [secret goes here…]`,
  )} creates a secret link instantly (of type *Text*). Example: ${Md.codeInline(
    '/scrt I love you!',
  )}`,
)}  

${Md.listBullet(
  `${Md.codeInline('/scrt text')}, ${Md.codeInline(
    '/scrt link',
  )} or ${Md.codeInline(
    '/scrt neogram',
  )} opens a dialog to create a specific type of secret.`,
)}`,
      }),
      Blocks.Divider(),
      Blocks.Context().elements(
        `Scrt.link is a tool to securely share sensitive information online. For more information and advanced usages visit https://scrt.link/slack.`,
      ),
    )
    .buildToObject()

export default getHelp
