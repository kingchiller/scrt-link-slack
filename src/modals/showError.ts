import { Modal, Blocks } from 'slack-block-builder'

type Error = {
  message?: string
}
const showError = ({ message }: Error) =>
  Modal({
    title: 'An error occured',
    submit: 'Close',
    callbackId: 'modal_error',
  })
    .blocks(
      Blocks.Section({
        text: message,
      }),
    )
    .buildToObject()

export default showError
