import { Modal, Blocks, Elements, Bits } from 'slack-block-builder'
import { SecretType } from 'scrt-link-core/dist/tsc/types'

const timerOptions = [3, 5, 10, 20, 30]
const secretTypes = [
  { text: 'Text', value: 'text' },
  { text: 'Link', value: 'url' },
  { text: 'Neogram™', value: 'neogram' },
]

export type CreateSecretMeta = {
  recipient?: string
}

type CreateSecretProps = {
  secretType?: SecretType
  recipient?: string // channel id
  showMenu?: boolean
}
const createSecret = (
  { secretType, recipient, showMenu }: CreateSecretProps = {
    secretType: 'text',
    showMenu: false,
  },
) =>
  Modal({
    title: 'Create a secret',
    submit: 'Create secret link',
    callbackId: 'modal_create_secret',
    privateMetaData: JSON.stringify({ recipient }), // Here we pass metadata to update modal view correctly
  })
    .blocks(
      Blocks.Section({
        text: `Let's create a one-time secret. You may include a password for extra security.`,
      }),

      recipient
        ? undefined
        : Blocks.Input({
            label: 'Recipient',
            hint: `It's usually not recommended to send secrets into a group conversation.`,
          })
            .blockId('recipient')
            .element(
              Elements.ConversationSelect({
                placeholder: 'Your confidant',
              })
                .actionId('recipient')
                .responseUrlEnabled(),
            ),
      showMenu || !secretType
        ? Blocks.Input({
            label: 'Type of Secret',
            hint: `Go to App Home to read more about the different types.`,
          })
            .blockId('secretType')
            .dispatchAction()
            .element(
              Elements.RadioButtons()
                .options(secretTypes.map((option) => Bits.Option(option)))
                .initialOption(
                  Bits.Option(
                    !secretType
                      ? secretTypes[0]
                      : secretTypes.find(({ value }) => value === secretType),
                  ),
                )
                .actionId('radio-switch-secret-type'),
            )
        : undefined,

      !secretType || secretType === 'text'
        ? Blocks.Input({ label: 'Your Secret' })
            .blockId('message')
            .element(
              Elements.TextInput({
                placeholder: 'Some confidential information…',
                //@ts-ignore
                multiline: true,
              }).actionId('message'),
            )
        : undefined,

      secretType === 'url'
        ? Blocks.Input({ label: 'URL' })
            .blockId('message')
            .hint('Think of it as a one time redirect.')
            .element(
              Elements.TextInput({
                placeholder: 'e.g. https://example.com',
              }).actionId('message'),
            )
        : undefined,

      secretType === 'neogram'
        ? Blocks.Input({ label: 'Neogram™' })
            .blockId('message')
            .hint('Animated message that self-destructs after some time.')
            .element(
              Elements.TextInput({
                placeholder: 'Some confidential information…',
                //@ts-ignore
                multiline: true,
              }).actionId('message'),
            )
        : undefined,

      Blocks.Input({ label: 'Password' })
        .blockId('password')
        .optional()
        .element(
          Elements.TextInput({
            placeholder: '*****',
          }).actionId('password'),
        ),

      ...(secretType === 'neogram'
        ? [
            Blocks.Input({ label: 'Destruction Message' })
              .blockId('neogramDestructionMessage')
              .optional()
              .element(
                Elements.TextInput({
                  placeholder: 'This message self-destructs in…',
                }).actionId('neogramDestructionMessage'),
              ),

            Blocks.Input({ label: 'Destruction Timeout' })
              .blockId('neogramDestructionTimeout')
              .optional()
              .element(
                Elements.StaticSelect()
                  .options(
                    timerOptions.map((item) =>
                      Bits.Option({
                        text: `${item} seconds`,
                        value: String(item),
                      }),
                    ),
                  )
                  .actionId('neogramDestructionTimeout'),
              ),
          ]
        : []),
    )
    .buildToObject()

export default createSecret
