## Scrt.link Slack App

**Scrt.link** lets you share one-time secrets with a link that self-destructs after the first visit.
[Slack App on scrt.link ](https://scrt.link/slack)

### Slack App

This Slack integration lets you easily create secrets from within your Slack conversations using a slash command `/scrt`.

👉 [Add to Slack](https://slack.scrt.link/slack/install)

#### Usage within Slack (Slash commands)

```bash
 /scrt # Opens modal with options
 /scrt …some secret message # Instant secret (text)
 /scrt [text, link, neogram] # Opens modal with specific type of secret
 /scrt help # Get help
```

### Development

First, add `.env` with ENV variables listed in `.env.example`.

```bash
# Install
yarn

# Build
yarn build

# Start app
yarn start

# Develop
yarn dev
```

### Deployment

App on Heroku -> https://scrt-link-slack.herokuapp.com/
Custom domain: https://slack.scrt.link

```bash
# Trigger deployment
git push heroku main
```

More info on Heroku deployment: https://slack.dev/bolt-js/deployments/heroku

### Resources

- [Bolt documentation](https://slack.dev/bolt)
- [MongoDB Quickstart ](https://github.com/mongodb-developer/nodejs-quickstart)
- [Slack Block Builder](https://www.npmjs.com/package/slack-block-builder)
- [Example Slack App: Tasks](https://github.com/slackapi/tasks-app/tree/main/nodejs)
- [Example Slack App: Account binding](https://github.com/slackapi/template-account-binding)

### Licence

MIT

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a>
